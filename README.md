# README #

#Project 2 - Version 1.0.0 -  Interpreting and Plotting Data


### What is this? ###

In this project we will be writing a set of python programs that will take a directory path as input and either: 
 
 A) change files in the designated directory to a format that will naturally open with Excel
 
 or
 
 B) graph data for a file or set of files in the designated path


### How do I get set up? ###

* Set Up Summary:

Programs used in this project will all be called from the main project directory, which contains both data file directories and all necessary programs and Readme files.

To make use of the programs, it is important to understand their designated uses.

Below are descriptors for each program in the project directory:

 -bending_plot.py: bending_plot takes converted files and plots designated data. Data plots will be able to be opened in Excel.
 -PyCel.py/PyCelTwo.py: <PyCel that works> is used to convert files contained in the Bending directory to an Excel- readable file format.
 -PlCel.py: PlCel is used to convert files contained in the Spectra directory to an Excel- readable file format.


* Configuration:

This program is run in python and the files are to be opened in Excel.


* Dependencies:

 matplotlib.pyplot
 numpy
 sys
 xlrdt 
 xlwt
 csv
 os
 path


* Database Configuration:

All information on this project can be found in the project home directory: p2-team4.


* How To Run Tests:

To run test type <python  <program name>.py> into bash and press return. When prompted, type in path to either Bending or Spectra and press return.


* Deployment Instructions:

To access these programs, navigate in shell to the project home directory. This is where you can run the python program for the task you wish to accomplish.


### Contribution guidelines ###

* Writing Tests:

This project deals with two specific and unique sets of data files found in the Bending and Spectra folders in this project directory. 
Any tests conducted using files that are not in the same format as the .raw files found in this repository may not produce expected results.
***PLEASE NOTE: USE ONLY PYCEL/PYCELTWO FOR CONVERSION OF BENDING FILES, AND PLCEL FOR CONVERSION OF SPECTRA FILES!!!


* Code Review:

Files in the project can be pulled at any time. Files can be accessed with vim directly from the project home directory. 


* Other Guidelines:

Refer to the repository Admin for further details.


### Who do I talk to? ###

* Repo Admin:

Eric Jankowski


* Other Team Contact:

Find contacts in contributors.txt 
