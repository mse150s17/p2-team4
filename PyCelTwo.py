# mypath should be the complete path for the directory containing the input text files
# Use this ONLY for RAW files!


import csv
import os
import sys
from sys import argv
from os import listdir
from os.path import isfile, join


mypath = input("Please enter the directory path for the input files: ")


sfiles = [ join(mypath,f) for f in listdir(mypath) if isfile(join(mypath,f)) and '.raw' in f]

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False   


for sfile in sfiles:
    k = open(sfile, 'r+')
    row_list = []
    

    reader = csv.reader(k)
    for row in reader:
#        print(row)
        row_list.append(row)
#        column_list = zip(*row_list)
#        print(column_list)
#        wr = csv.writer('.csv'.join(open(sfile, 'w') ))
        wr.writerow(reader)

    k.close()








#\n is EOL character
