#This is to make the extensionless files compatible with Excel. Note that it is not necessary to have this save an .xls file; only to make the file open up with excel and be read without any funny business...
#This is the best I can come up with until PyCel is fixed. This literally allows you to run the command, then click on the files, and they will open up in excel.
#ONCE AGAIN: THIS IS ONLY FOR USE ON THE SPECTRA FILES AND WILL OVERWRITE THEM. THIS IS A QUICK AND DIRTY SOLUTION, I KNOW...
import csv
import os
import sys
import matplotlib.pyplot as plt
from sys import argv
from os import listdir
from os.path import isfile, join
from pathlib import Path

mypath = input("Please enter the directory path for the input files: ")


mf = [join(mypath,f) for f in listdir(mypath)]
for filename in mf:
    os.rename(filename, filename + ".xls")


plt.plot(3,7);
plt.xlabel('Strain');
plt.ylabel('Stress (MPa)');
plt.title('Stress vs. Strain');
plt.show()



#good for strings f = open()
#good for strings 	for line in f:
#good for strings 		print(line)
#good for strings 		line.split(',')

