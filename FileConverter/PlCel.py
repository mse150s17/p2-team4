#This is to make the extensionless files compatible with Excel. Note that it is not necessary to have this save an .xls file; only to make the file open up with excel and be read without any funny business...
#This is the best I can come up with until PyCel is fixed. This literally allows you to run the command, then click on the files, and they will open up in excel.
#ONCE AGAIN: THIS IS ONLY FOR USE ON THE SPECTRA FILES AND WILL OVERWRITE THEM. THIS IS A QUICK AND DIRTY SOLUTION, I KNOW...

## Important Below *********
#Edited to make it remove old file extensions. Doing so makes the files pop into current directory which was bad. The solution to this is to cp PlCel.py to the desired directory each time it is needed to run. 
import csv
import os
import sys
import shutil
from sys import argv
from os import listdir
from os.path import isfile, join
from pathlib import Path

mypath = os.getcwd()


mf = [join(mypath,f) for f in listdir(mypath)]
for filename in mf:
    os.rename(filename, filename[:-4] + ".csv")
    
os.remove("PlCe.csv")


#good for strings f = open()
#good for strings 	for line in f:
#good for strings 		print(line)
#good for strings 		line.split(',')
