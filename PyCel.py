# mypath should be the complete path for the directory containing the input text files
# Use this ONLY for RAW files!

from os import listdir
from os.path import isfile, join


mypath = input("Please enter the directory path for the input files: ")


rawfiles = [ join(mypath,f) for f in listdir(mypath) if isfile(join(mypath,f)) and  '.raw' in  f]

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False        

import xlwt
import xlrd

style = xlwt.XFStyle()
style.num_format_str = '#,###0.00'  

for rawfile in rawfiles:
    f = open(rawfile, 'r+')
    row_list = []
    for row in f:
        row_list.append(row.split('|'))
    column_list = zip(*row_list)
    workbook = xlwt.Workbook()
    worksheet = workbook.add_sheet('Sheet1')
    i = 0
    for column in column_list:
        for item in range(len(column)):
            value = column[item].strip()
            if is_number(value):
                worksheet.write(item, i, float(value), style=style)
            else:
                worksheet.write(item, i, value)
        i+=1
    workbook.save(rawfile.replace('.raw', '.xls'))
#READ: Okay, so far, this thing is driving me nuts; it works, but you get a long damn column, AND the entries are all in "" marks.
#I started another version from scratch, but IT makes a long damn row, this time enclosed by brackets.

        return True
        return True
    except ValueError:
        return False        

    except ValueError:
        return False        

ply.plot(file) 
xlabel('strain');
ylabel('stress (MPa)');
title('Stress vs. Strain');



